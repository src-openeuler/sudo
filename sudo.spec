Name: sudo
Version: 1.9.16p2
Release: 1
Summary: Allows restricted root access for specified users
License: ISC
URL: https://www.sudo.ws

Source0: https://www.sudo.ws/dist/%{name}-%{version}.tar.gz
Source1: sudoers
Source2: sudo
Source3: sudo-i

Patch0:  Fix-compilation-error-on-sw64-arch.patch
Requires: pam
Recommends: vim-minimal
Requires(post): coreutils

BuildRequires: pam-devel groff openldap-devel flex bison automake autoconf libtool
BuildRequires: audit-libs-devel libcap-devel libselinux-devel sendmail gettext zlib-devel
BuildRequires: chrpath

%description
Sudo is a program designed to allow a sysadmin to give limited root privileges
to users and log root activity. The basic philosophy is to give as few
privileges as possible but still allow people to get their work done.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains header files developing sudo
plugins that use %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -I m4 -fv --install
export CFLAGS="%{build_cflags} -fpie" LDFLAGS="%{build_ldflags} -pie"
%configure \
        --prefix=%{_prefix} \
        --sbindir=%{_sbindir} \
        --libdir=%{_libdir} \
        --docdir=%{_pkgdocdir} \
        --disable-root-mailer \
        --disable-intercept \
        --disable-log-server \
        --disable-log-client \
        --with-logging=syslog \
        --with-logfac=authpriv \
        --with-pam \
        --with-pam-login \
        --with-editor=/bin/vi \
        --with-env-editor \
        --with-ignore-dot \
        --with-tty-tickets \
        --with-ldap \
        --with-selinux \
        --with-passprompt="[sudo] password for %p: " \
        --with-linux-audit \
        --with-sssd

%make_build

%check
%make_build check

%install
%make_install install_uid=`id -u` install_gid=`id -g` sudoers_uid=`id -u` sudoers_gid=`id -g`

chmod 755 $RPM_BUILD_ROOT%{_bindir}/* $RPM_BUILD_ROOT%{_sbindir}/*
install -p -d -m 700 $RPM_BUILD_ROOT/var/db/sudo
install -p -d -m 700 $RPM_BUILD_ROOT/var/db/sudo/lectured
install -p -d -m 750 $RPM_BUILD_ROOT/etc/sudoers.d
install -p -c -m 0440 %{S:1} $RPM_BUILD_ROOT/etc/sudoers
install -p -d -m 755 $RPM_BUILD_ROOT/etc/dnf/protected.d/

touch sudo.conf
echo sudo > sudo.conf
install -p -c -m 0644 sudo.conf $RPM_BUILD_ROOT/etc/dnf/protected.d/
rm -f sudo.conf

chmod +x $RPM_BUILD_ROOT%{_libexecdir}/sudo/*.so

rm -rf $RPM_BUILD_ROOT%{_pkgdocdir}/LICENSE
rm -rf $RPM_BUILD_ROOT%{_datadir}/examples/sudo

%delete_la

rm -f $RPM_BUILD_ROOT%{_sysconfdir}/sudoers.dist

%chrpath_delete
mkdir -p $RPM_BUILD_ROOT/etc/ld.so.conf.d
echo "/usr/libexec/sudo" > $RPM_BUILD_ROOT/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%find_lang sudo
%find_lang sudoers

mkdir -p $RPM_BUILD_ROOT/etc/pam.d
install -p -c -m 0644 %{S:2} $RPM_BUILD_ROOT/etc/pam.d/sudo
install -p -c -m 0644 %{S:3} $RPM_BUILD_ROOT/etc/pam.d/sudo-i

rm -f %{buildroot}%{_pkgdocdir}/ChangeLog

%post
/bin/chmod 0440 /etc/sudoers || :
/sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f sudo.lang -f sudoers.lang
%attr(0440,root,root) %config(noreplace) /etc/sudoers
%attr(0750,root,root) %dir /etc/sudoers.d/
%attr(0644,root,root) %{_tmpfilesdir}/sudo.conf
%attr(0644,root,root) %config(noreplace) /etc/dnf/protected.d/sudo.conf
%attr(0640,root,root) %config(noreplace) /etc/sudo.conf
%attr(4111,root,root) %{_bindir}/sudo
%attr(0111,root,root) %{_bindir}/sudoreplay
%{_bindir}/sudoedit
%{_bindir}/cvtsudoers
%attr(0755,root,root) %{_sbindir}/visudo
%attr(0755,root,root) %{_libexecdir}/sudo/sesh
%attr(0644,root,root) %{_libexecdir}/sudo/sudo_noexec.so
%attr(0644,root,root) %{_libexecdir}/sudo/sudoers.so
%attr(0644,root,root) %{_libexecdir}/sudo/group_file.so
%attr(0644,root,root) %{_libexecdir}/sudo/system_group.so
%attr(0644,root,root) %{_libexecdir}/sudo/audit_json.so
%attr(0644,root,root) %{_libexecdir}/sudo/libsudo_util.so.?.?.?
%{_libexecdir}/sudo/libsudo_util.so.?
%{_libexecdir}/sudo/libsudo_util.so
%dir /var/db/sudo
%dir /var/db/sudo/lectured
%dir %{_libexecdir}/sudo
%config(noreplace) /etc/pam.d/sudo
%config(noreplace) /etc/pam.d/sudo-i
%config(noreplace) /etc/ld.so.conf.d/*
%license LICENSE.md

%files devel
%{_includedir}/sudo_plugin.h

%files help
%dir %{_pkgdocdir}/
%{_mandir}/man5/*
%{_mandir}/man8/*
%{_mandir}/man1/*
%{_pkgdocdir}/*
%doc plugins/sample/sample_plugin.c

%changelog
* Sat Nov 30 2024 Funda Wang <fundawang@yeah.net> - 1.9.16p2-1
- update to 1.9.16p2

* Sun Nov 17 2024 Funda Wang <fundawang@yeah.net> - 1.9.16p1-1
- update to 1.9.16p1

* Sun Sep 15 2024 Funda Wang <fundawang@yeah.net> - 1.9.16-1
- update to 1.9.16

* Mon Jan 8 2024 wangqingsan <wangqingsan@huawei.com> - 1.9.15p5-1
- Upgrade to 1.9.15p5

* Wed Jul 19 2023 zhoushuiqing <zhoushuiqing2@huawei.com> - 1.9.14p1-1
- Upgrade to 1.9.14p1

* Wed Apr 12 2023 wangyu <wangyu283@huawei.com> - 1.9.12p2-5
- Fix compilation error on sw64 arch.

* Tue Mar 28 2023 wangcheng <wangcheng156@huawei.com> - 1.9.12p2-4
- Fix CVE-2023-28486 and CVE-2023-28487

* Fri Mar 10 2023 wangyu <wangyu283@huawei.com> - 1.9.12p2-3
- Fix CVE-2023-27320.

* Mon Mar 06 2023 wangyu <wangyu283@huawei.com> - 1.9.12p2-2
- Remove -rpath option.

* Tue Jan 31 2023 wangyu <wangyu283@huawei.com> - 1.9.12p2-1
- Upgrade to 1.9.12p2

* Thu Jan 19 2023 houmingyong<houmingyong@huawei.com> - 1.9.8p2-8
- Fix CVE-2023-22809

* Thu Dec 08 2022 wangyu <wangyu283@huawei.com> - 1.9.8p2-7
- Backport patches from upstream community

* Fri Nov 25 2022 wangyu <wangyu283@huawei.com> - 1.9.8p2-6
- Backport patches from upstream community

* Wed Nov 23 2022 wangyu <wangyu283@huawei.com> - 1.9.8p2-5
- Backport patches from upstream community

* Sat Nov 5 2022 wangyu <wangyu283@huawei.com> - 1.9.8p2-4
- Fix CVE-2022-43995

* Fri Sep 2 2022 wangyu <wangyu283@huawei.com> - 1.9.8p2-2
- Fix CVE-2022-37434 and CVE-2022-33070

* Tue Feb 15 2022 panxiaohe <panxh.life@foxmail.com> - 1.9.8p2-1
- Update to 1.9.8p2

* Thu Sep 16 2021 yixiangzhike <zhangxingliang3@huawei.com> - 1.9.5p2-3
- DESC: treat stack exhaustion like memory allocation failure

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.9.5p2-2
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Wed Jul 7 2021 panxiaohe <panxiaohe@huawei.com> - 1.9.5p2-1
- Update to 1.9.5p2

* Fri Jan 29 2021 zoulin <zoulin13@huawei.com> - 1.9.2-3
- Fix runstatedir handling for distros that do not support it

* Wed Jan 27 2021 panxiaohe <panxiaohe@huawei.com> - 1.9.2-2
- fix CVE-2021-23239 CVE-2021-23240 CVE-2021-3156

* Wed Jul 29 2020 zhangxingliang <zhangxingliang3@huawei.com> - 1.9.2-1
- update to 1.9.2

* Fri Apr 17 2020 Anakin Zhang <nbztx@126.com> - 1.8.27-5
- Read drop-in files from /etc/sudoers.d

* Mon Jan 20 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.8.27-4
- fix CVE-2019-19232 and CVE-2019-19234

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.8.27-3
- clean code

* Mon Dec 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.8.27-2
- Fix CVE-2019-14287

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.8.27-1
- Package init
